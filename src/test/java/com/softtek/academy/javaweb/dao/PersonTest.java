package com.softtek.academy.javaweb.dao;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.PersonDao;

public class PersonTest {

	@Autowired
	private PersonDao personDao;
	@BeforeEach
	public void initJDBC() {
		this.personDao=new PersonDaoImpl();
	}
	@Test
	public void testGetAllPersons() {
		//setup
		Person color= new Person("victor","12",1);
		//execute
		List<Person> persons=personDao.getAll();
		//verify
		
		assertNotNull(persons,"Persons list is empty");
		
		Person actualColor =persons.get(0);
		assertEquals(color.getName(),actualColor.getName(),"Colors do not match");
	}
}
