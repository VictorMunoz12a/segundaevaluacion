package com.softtek.academy.javaweb.Spring;

import java.util.List;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.PersonDaoImpl;
import com.softtek.academy.javaweb.service.PersonServiceImpl;

public class App {

	public static void main(String[] args) {
		System.out.println("MySQL JDBC Connection");
    	PersonDaoImpl colorDao = new PersonDaoImpl();
    	PersonServiceImpl colorService 
    		= new PersonServiceImpl(colorDao);
    	
    	List<Person> persons = colorService.getAll();
    	persons.forEach(System.out::println);
    }
	}


