package com.softtek.academy.javaweb.beans;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Person {
	private String name;
	private String age;
	@Id
	private long id;
	
	public Person(String name, String age, long i) {
		super();
		this.name = name;
		this.age = age;
		this.id = i;
	}
	
	public Person() {
		
	}
	
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
}
