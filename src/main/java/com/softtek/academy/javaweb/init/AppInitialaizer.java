package com.softtek.academy.javaweb.init;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.softtek.academy.javaweb.conexion.Conexion;;

public class AppInitialaizer {
	public void onStartup(ServletContext container) throws ServletException {

        AnnotationConfigWebApplicationContext ctx =
        		new AnnotationConfigWebApplicationContext();
        ctx.register(Conexion.class);
        ctx.setServletContext(container);

        ServletRegistration.Dynamic servlet = container.addServlet("dispatcher",
             new DispatcherServlet(ctx));

        servlet.setLoadOnStartup(1);
        servlet.addMapping("/");
    }
}
