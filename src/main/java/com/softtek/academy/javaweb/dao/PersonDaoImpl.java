package com.softtek.academy.javaweb.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;


import com.softtek.academy.javaweb.beans.Person;

public class PersonDaoImpl implements PersonDao {
	private JdbcTemplate jdbcTemplate;
	@Override
	public List<Person> getAll() {
		// TODO Auto-generated method stub
		String SQL_SELECT = "Select * from Person";
		List<Person> result = jdbcTemplate
				.query(SQL_SELECT, (rs, rowNum) -> {
			Person person = new Person(
					rs.getString("uname"),
					rs.getString("age"),
					rs.getLong("studentID")
					);
			return person;
		});
		return result;
	}

	@Override
	public Person getById(long id) {
		String SQL_SELECT = "Select * from Person where studentID = ?";
		Person person = jdbcTemplate
				.queryForObject(SQL_SELECT, new Object[] {id},
					new BeanPropertyRowMapper <Person>	(Person.class));
		return person;
	}
	@Override
	public int saveStudent(Person e) {
		 String query="insert into Person values('"+e.getName()+"','"+e.getAge()+"','"+e.getId()+"')";  
		    return jdbcTemplate.update(query);
	
	}
	
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	

}
