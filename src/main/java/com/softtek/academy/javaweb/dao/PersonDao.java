package com.softtek.academy.javaweb.dao;

import java.util.List;


import com.softtek.academy.javaweb.beans.Person;

public interface PersonDao {
	List<Person> getAll();

	Person getById(long id);
	
	int saveStudent (Person e);
	
}
