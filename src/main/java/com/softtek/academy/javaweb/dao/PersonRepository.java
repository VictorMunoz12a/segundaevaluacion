package com.softtek.academy.javaweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.softtek.academy.javaweb.beans.Person;

public class PersonRepository implements PersonDao {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Person> getAll() {
		// TODO Auto-generated method stub
		return (List <Person>)entityManager
				.createQuery("Select c from Person", Person.class).getResultList();
	}

	@Override
	public Person getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int saveStudent(Person e) {
		// TODO Auto-generated method stub
		return 0;
	}

}
