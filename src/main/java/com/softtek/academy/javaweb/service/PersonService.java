package com.softtek.academy.javaweb.service;

import java.util.List;


import com.softtek.academy.javaweb.beans.Person;

public interface PersonService {
List<Person> getAll();
	
	Person getById(long id);

	List<Person> getFilterList();
}
