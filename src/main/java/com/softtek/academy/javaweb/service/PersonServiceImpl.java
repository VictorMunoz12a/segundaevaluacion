package com.softtek.academy.javaweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.PersonDao;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {
	@Autowired
	@Qualifier("personrepository")
	private PersonDao personDao;
	
	public PersonServiceImpl(PersonDao personDao) {
		super();
		this.personDao = personDao;
	}

	@Override
	public List<Person> getAll() {
		// TODO Auto-generated method stub
		return personDao.getAll();
	}

	@Override
	public Person getById(long id) {
		// TODO Auto-generated method stub
		return personDao.getById(id);
	}

	@Override
	public List<Person> getFilterList() {
		// TODO Auto-generated method stub
		List<Person> person= personDao.getAll();
		person.removeIf((c)->c.getName().equals("Victor"));
		
		return person;
	}

}
